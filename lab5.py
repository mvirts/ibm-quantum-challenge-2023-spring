
from typing import List, Optional

from qiskit import transpile, QuantumCircuit, QuantumRegister, ClassicalRegister
from qiskit.result import marginal_counts
import itertools
import numpy as np
import warnings

import sklearn.ensemble

warnings.filterwarnings("ignore")

import math

pi = math.pi

# Preparing registers
def create_registers():
    return QuantumRegister(127),[ClassicalRegister(127)]
# quantum_register = QuantumRegister(127)
# classical_register1 = ClassicalRegister(127)
# classical_register2 = ClassicalRegister(63)
# crs = [classical_register1]#,classical_register2]

# For simplicity we map the physical qubits to the logical qubits directly using the same number.
initial_layout = [
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
    36,
    37,
    38,
    39,
    40,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50,
    51,
    52,
    53,
    54,
    55,
    56,
    57,
    58,
    59,
    60,
    61,
    62,
    63,
    64,
    65,
    66,
    67,
    68,
    69,
    70,
    71,
    72,
    73,
    74,
    75,
    76,
    77,
    78,
    79,
    80,
    81,
    82,
    83,
    84,
    85,
    86,
    87,
    88,
    89,
    90,
    91,
    92,
    93,
    94,
    95,
    96,
    97,
    98,
    99,
    100,
    101,
    102,
    103,
    104,
    105,
    106,
    107,
    108,
    109,
    110,
    111,
    112,
    113,
    114,
    115,
    116,
    117,
    118,
    119,
    120,
    121,
    122,
    123,
    124,
    125,
    126,
]
# The "even"" qubits will be used for the 54 qubit GHZ-state
ghz_qubits = [
    # 0,
    # 2,
    4,
    6,
    8,
    10,
    12,
    # 18,
    20,
    22,
    24,
    26,
    28,
    30,
    32,
    37,
    39,
    41,
    43,
    45,
    47,
    49,
    51,
    56,
    58,
    60,
    62,
    64,
    66,
    68,
    70,
    75,
    77,
    79,
    81,
    83,
    85,
    87,
    89,
    94,
    96,
    98,
    100,
    102,
    104,
    106,
    # 108,
    114,
    116,
    118,
    120,
    122,
    # 124,
    # 126,
]
# The "odd" qubits will be used as the stabilizers
stabilizer_qubits = [
    # 1,
    # 3,
    5,
    7,
    9,
    11,
    # 14,
    15,
    16,
    17,
    # 19,
    21,
    23,
    25,
    27,
    29,
    31,
    33,
    34,
    35,
    36,
    38,
    40,
    42,
    44,
    46,
    48,
    50,
    52,
    53,
    54,
    55,
    57,
    59,
    61,
    63,
    65,
    67,
    69,
    71,
    72,
    73,
    74,
    76,
    78,
    80,
    82,
    84,
    86,
    88,
    90,
    91,
    92,
    93,
    95,
    97,
    99,
    101,
    103,
    105,
    # 107,
    109,
    110,
    111,
    # 112,
    115,
    117,
    119,
    121,
    # 123,
    # 125,
]


# In[2]:


from qiskit_ibm_provider import IBMProvider
from qiskit import transpile

def get_backend():
    provider = IBMProvider()
    instance='qc-spring-23-7/group-3/rec8WCjMdho34F3Dz'


    backend_name = "ibm_sherbrooke"
    backend = provider.get_backend(backend_name, instance=instance)
    return backend

def get_distance_matrix(backend):
    return backend.coupling_map.distance_matrix


def generate_ghz127(quantum_register,crs,center_node, dm):
    qc = QuantumCircuit(quantum_register, *crs)
    qr = quantum_register
    
    node_included = [False]*len(qr)
    
    def take_node(x):
        node_included[x] = True
        return x
    
    def check_node(x):
        return not node_included[x]

    root = take_node(center_node)
    
    cxlist = []
    
    nodelist = [root]
    new_nodelist = []
    node_levels = {}
    level = 0
    while nodelist and not all(node_included):
        # print(nodelist)
        node_levels[level] = nodelist
        for node in nodelist:
            for other,dist in enumerate(dm[node,:]):
                if dist == 1:
                    if check_node(other):
                        # print(node,other)
                        take_node(other)
                        cxlist.append((node,other))
                        new_nodelist.append(other)

                    
        level += 1
        nodelist = new_nodelist
    qc.h(qr[root])
    for a,b in cxlist:
        qc.cx(qr[a],qr[b])
    
    return qc, cxlist

# use with lambda x: generate_ghz127(quantum_register,crs,x)
def find_min_depth(fn):
    min_cn=0
    min_depth=127
    # for center_node in range(len(quantum_register)):
    for center_node in ghz_qubits:

        ghz_circuit, cxlist = fn(center_node)
        d=ghz_circuit.depth()
        if d<min_depth:
            min_depth= d
            min_cn = center_node
    center_node = min_cn
    return fn(center_node)



def deentangle_qubits(quantum_register, crs, cxlist, dm):
    qc = QuantumCircuit(quantum_register, *crs)

    qr = quantum_register
    ####### your code goes here #######
#     for n in stabilizer_qubits:
#         parents = list(map(lambda a: a[0],filter(lambda a: a[1]==n,cxlist)))
#         children = list(map(lambda a: a[1],filter(lambda a: a[0]==n,cxlist)))
#         if children:
#             print(n,"c")
#             qc.cx(qr[children[0]],qr[n])
#         else:
            
#             print(n,"p")
#             qc.cx(qr[n],qr[parents[0]])
    from collections import defaultdict
    ucxlist = []
    source_counts = defaultdict(int)
    sq = stabilizer_qubits
    for q in [x[1] for x in reversed(cxlist) if x[1] not in ghz_qubits]:
        parents = list(map(lambda a: a[0],filter(lambda a: a[1]==q,cxlist)))
        sources = [xi for xi,x in enumerate(dm[q,:]) if x == 1 and ((q not in sq) or (xi in ghz_qubits))]
        sf = None
        sf_count = 127
        for s in sources:
            if source_counts[s]<sf_count and ((q not in sq) or ((s,q) not in cxlist)):
                sf_count = source_counts[s]
                sf = s
        
        if len(parents)%2==1:
            source_counts[sf]+=1
            qc.cx(qr[sf],qr[q])
            ucxlist.append((sf,q))
        # else:
            # print("skippied")
            # print(q,parents)
    # for a,b in source_counts.items():
        # print(a,b)
#     children = [list(map(lambda a: a[1],filter(lambda a: a[0]==n,cxlist))) for n in range(len(qr))]
#     leaves = [ix for ix,x in enumerate(children) if len(x)==0]
    
#     # leaves = node_levels[max(node_levels.keys())]
#     for a,b in reversed(cxlist):
#         if a in sq and b not in sq:
#             print(a,b)
#             qc.cx(qr[b],qr[a])
#         if a not in sq and b in sq and b in leaves:
#             print("bleaves",a,b)
#             qc.cx(qr[a],qr[b])
#         if a in sq and b in sq:
#             print("wat")
#             # qc.cx(qr[a],qr[b])

    return qc, ucxlist
# from collections import defaultdict

# c = defaultdict(int)
# for a,b in cxlist:
#     c[a]+=1
# print(c)




def check_cxcounts(cxlist,ucxlist):
    from collections import Counter

    return Counter([x[1] for x in cxlist+ucxlist if x[1] not in ghz_qubits])


# Measuring stabilizers this can also be used in post processing to see what went wrong.
# import itertools
# rpairs=list(zip(quantum_register,itertools.chain(*crs)))

def measure_all(quantum_register, crs):
    qc = QuantumCircuit(quantum_register, *crs)
    qc.measure(quantum_register,crs[0])
    return qc



def measure_stabilizers(quantum_register, crs):
    qc = QuantumCircuit(quantum_register, *crs)
    for ix,qix in enumerate(stabilizer_qubits):
        qc.measure(quantum_register[qix],crs[0][ix])
    return qc

def measure_ghz(quantum_register, crs):
    qc = QuantumCircuit(quantum_register, *crs)
    for ix,qix in enumerate(ghz_qubits):   
        qc.measure(quantum_register[qix],crs[0][ix])
    return qc



number_of_shots: int = 1024

def transpile_defaults(qc,backend):
    return transpile(qc, backend, initial_layout=initial_layout)

def run_memory(qc, backend):
    return backend.run(
        qc,
        shots=1024,
        memory=True,
        job_tags=["ghz_state", "spring_challenge"],
    )

def run_dynamic(qc,backend):

    job = backend.run(
        qc,
        dynamic=True,
        job_tags=["dynamic", "spring_challenge"],
    )



def get_job_status(job_id):
    job = provider.backend.retrieve_job(job_id)
    return job.status()



def get_job_memory(job_id):
    data = provider.backend.retrieve_job(job_id).result().get_memory()
    import pickle
    with open(f'data_{job_id}.pkl','rb') as fh:
        data=pickle.load(fh)

# A function to test the quality of a GHZ-state. The lower the better

def score(dat):
        avg = sum(dat)/len(dat)
        t = round(avg)
        dif = abs(avg-t)
        return -math.log(1-dif*(2-1e-5))


def test_ghz(data):
    
    ####### your code goes here #######
    ghz_data = [[int(s[x]) for x in ghz_qubits] for s in data]
    run_score = map(score,ghz_data)
    avg = sum(run_score)/len(data)
    return avg


def correct(data):
    
    ####### your code goes here #######
    ghz_data = [[int(s[x]) for x in ghz_qubits] for s in data]
    stabilizer_data = [[int(s[x]) for x in stabilizer_qubits] for s in data]

    rfc = [sklearn.tree.DecisionTreeClassifier() for _ in ghz_data[0]]
    
    targets = [round(sum(x)/len(x)) for x in ghz_data]
    
    flip_data = [[v!=t for v in x] for x,t in zip(ghz_data,targets)]
    
    for c,d in zip(rfc,zip(*flip_data)):
        c.fit(stabilizer_data,d)
    
    cgd = []
    for gd,sd in zip(ghz_data,stabilizer_data):
        g = gd.copy()
        for ix,c in enumerate(rfc):
            if c.predict([sd]):
                g[ix] = 0 if g[ix] == 1 else 1
        cgd.append(g)
                
    run_score = map(score,cgd)
    avg = sum(run_score)/len(data)
    return avg, rfc
    


def correctioncircuit(qr:QuantumRegister, crs,dtc):
    qc = QuantumCircuit(qr,*crs)
    
    def mkifs(node,target,dt):
        
        feature = dt.tree_.feature
        values = dt.tree_.value
        classes = dt.classes_
        children_left = dt.tree_.children_left
        children_right = dt.tree_.children_right
        
        if feature[node] == -2:
            if classes[np.argmax(values[node])]:
                qc.x(target)
        else:
            cix = feature[node]
            with qc.if_test((classical_register1[cix],1)) as _else:
                mkifs(children_right[node],target,dt)
            with _else:
                mkifs(children_left[node],target,dt) 
        

    for ix,dt in enumerate(dtc):
        mkifs(0,qr[ix],dt)
    
    return qc


def correctioncircuit2(qr,crs,cxlist,ucxlist):
    qc = QuantumCircuit(qr,*crs)
    for qtarget in ghz_qubits:
        stabs = list(map(lambda x: x[1],filter(lambda x: qtarget == x[0],itertools.chain(cxlist,ucxlist))))
        def ifall(stabs):
            if len(stabs)>0:
                v = stabs[0]
                if v in stabilizer_qubits:
                    with qc.if_test((crs[0][stabilizer_qubits.index(v)],1)):
                        ifall(stabs[1:])
                else:
                    ifall(stabs[1:])
            else:
                qc.x(qtarget)
        ifall(stabs)
    return qc